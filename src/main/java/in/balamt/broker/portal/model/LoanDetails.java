package in.balamt.broker.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "loan_details")
@JsonInclude(Include.NON_NULL)
public class LoanDetails extends ResourceSupport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "loan_id")
	Long loanid;

	String loanType;
	double loanAmount;
	float loanTenure;
	float intrest;
	Date createdDate;
	Date fundedDate;

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public float getLoanTenure() {
		return loanTenure;
	}

	public void setLoanTenure(float loanTenure) {
		this.loanTenure = loanTenure;
	}

	public float getIntrest() {
		return intrest;
	}

	public void setIntrest(float intrest) {
		this.intrest = intrest;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getFundedDate() {
		return fundedDate;
	}

	public void setFundedDate(Date fundedDate) {
		this.fundedDate = fundedDate;
	}

	public Long getLoanId() {
		return loanid;
	}

	public void setLoanId(Long loanId) {
		this.loanid = loanId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LoanDetails [loanid=");
		builder.append(loanid);
		builder.append(", loanType=");
		builder.append(loanType);
		builder.append(", loanAmount=");
		builder.append(loanAmount);
		builder.append(", loanTenure=");
		builder.append(loanTenure);
		builder.append(", intrest=");
		builder.append(intrest);
		builder.append(", createdDate=");
		builder.append(createdDate);
		builder.append(", fundedDate=");
		builder.append(fundedDate);
		builder.append("]");
		return builder.toString();
	}

}
