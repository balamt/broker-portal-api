package in.balamt.broker.portal.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.balamt.broker.portal.model.Deal;

@Repository
@Transactional
public interface LoanRepository extends JpaRepository<Deal, Long> {

}
