package in.balamt.broker.portal;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
@EnableJpaRepositories
@ComponentScan(basePackages = { "in.balamt" })
public class BrokerPortalApiApplication {

	private static final String ENG_TRAINEDDATA = "eng.traineddata";
	public static final Logger LOGGER = Logger.getLogger(BrokerPortalApiApplication.class.getName());

	public static void main(String[] args) {

		try {
			InitilizeTestData();
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}

		SpringApplication.run(BrokerPortalApiApplication.class, args);
	}

	private static void InitilizeTestData() throws Exception {
		String executionPath = System.getProperty("user.dir");
		String targetFile = String.format("%s/%s", executionPath, ENG_TRAINEDDATA);
		InputStream sourceFileAsStream = getResource(ENG_TRAINEDDATA);
		File targetDataFile = new File(targetFile);
		Files.copy(sourceFileAsStream, targetDataFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}

	public static InputStream getResource(String resource) throws Exception {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		return cl.getResourceAsStream(resource);
	}

}
