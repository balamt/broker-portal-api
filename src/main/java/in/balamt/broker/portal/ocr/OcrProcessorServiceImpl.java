package in.balamt.broker.portal.ocr;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.leptonica.PIX;
import org.bytedeco.leptonica.global.lept;
import org.bytedeco.tesseract.TessBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import in.balamt.broker.portal.FileStorageProperties;
import in.balamt.broker.portal.model.Customer;
import in.balamt.broker.portal.model.Deal;
import in.balamt.broker.portal.model.Employment;
import in.balamt.broker.portal.model.LoanDetails;
import in.balamt.broker.portal.ocr.exception.FileStorageException;
import in.balamt.broker.portal.ocr.exception.MyFileNotFoundException;
import in.balamt.broker.portal.ocr.exception.OCRInitializationException;
import in.balamt.broker.portal.ocr.util.OcrUtil;
import in.balamt.broker.portal.repository.LoanRepository;

@Service
public class OcrProcessorServiceImpl implements OcrProcessorService {

	public static final Logger LOGGER = Logger.getLogger(OcrProcessorServiceImpl.class.getName());
	private final String executionPath = System.getProperty("user.dir");
	private final Path fileStorageLocation;

	@Autowired
	LoanRepository loanRepository;

	@Autowired
	public OcrProcessorServiceImpl(FileStorageProperties fileStorageProperties) throws FileStorageException {
		if (fileStorageProperties == null) {
			LOGGER.log(Level.SEVERE, "fileProp Not loaded");
		}
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}
	}

	@Override
	public Deal getApplicationContent(MultipartFile file) throws FileStorageException, OCRInitializationException {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// TODO: This has to be eliminated, when we have the authorized user or role
			// based authorization implemented.
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}

			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			/**
			 * Initializing the OCR
			 */

			BytePointer outText;
			TessBaseAPI api = new TessBaseAPI();

			if (api.Init(executionPath, "eng") != 0) {
				LOGGER.severe("Could not initialize tesseract.");
				System.exit(-1);
				throw new OCRInitializationException("Could not initialize tesseract.");
			}

			// Open input image with leptonica library
			PIX image = lept.pixRead(targetLocation.toString());
			api.SetImage(image);
			// Get OCR result
			outText = api.GetUTF8Text();

			LOGGER.info("Scanned Content " + outText.getString());

			Customer customer = OcrUtil.MapCustomerData(outText.getString());
			Employment employment = OcrUtil.MapEmploymentData(outText.getString());
			LoanDetails loanDetail = OcrUtil.MapLoanData(outText.getString());
			customer.setEmployment(employment);

			// Destroy used object and release memory
			api.End();
			outText.deallocate();
			lept.pixDestroy(image);

			/**
			 * End of Processing the image
			 */

			Deal deal = new Deal();
			deal.setCustomer(customer);
			deal.setLoanDetails(loanDetail);
			Deal returnDeal = loanRepository.save(deal);
			return returnDeal;
		} catch (Exception ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	@Override
	public List<String> listAllFilesFromUploads() throws IOException, MyFileNotFoundException {
		return getResourceFiles(executionPath + "/uploads");
	}

	private List<String> getResourceFiles(String path) throws IOException {
		LOGGER.info("Path " + path);
		List<String> filenames = new ArrayList<>();
		File folderPath = new File(path);
		final File[] fileList = folderPath.listFiles();

		for (File f : fileList) {
			LOGGER.info("NAME:" + f.getName());
			filenames.add(f.getName());
		}
		return filenames;
	}

	private InputStream getResourceAsStream(String resource) {
		final InputStream in = getContextClassLoader().getResourceAsStream(resource);
		return in == null ? getClass().getResourceAsStream(resource) : in;
	}

	private ClassLoader getContextClassLoader() {
		return Thread.currentThread().getContextClassLoader();
	}

	@Override
	public Deal getDeal(String dealNumber) throws IOException {
		Deal deal = new Deal();
		try {
			deal = loanRepository.findById(Long.parseLong(dealNumber)).get();
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
		}
		return deal;
	}

	@Override
	public Resource loadFileAsResource(String fileName) throws MyFileNotFoundException {
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new MyFileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new MyFileNotFoundException("File not found " + fileName, ex);
		}
	}

}
