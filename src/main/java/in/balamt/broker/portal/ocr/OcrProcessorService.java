package in.balamt.broker.portal.ocr;

import java.io.IOException;
import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import in.balamt.broker.portal.model.Deal;
import in.balamt.broker.portal.ocr.exception.FileStorageException;
import in.balamt.broker.portal.ocr.exception.MyFileNotFoundException;
import in.balamt.broker.portal.ocr.exception.OCRInitializationException;

@Service
public interface OcrProcessorService {

	public Deal getApplicationContent(MultipartFile file) throws FileStorageException, OCRInitializationException;

	public Deal getDeal(String dealNumber) throws IOException;

	List<String> listAllFilesFromUploads() throws IOException, MyFileNotFoundException;

	public Resource loadFileAsResource(String fileName) throws MyFileNotFoundException;

}
