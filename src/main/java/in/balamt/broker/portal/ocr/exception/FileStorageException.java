package in.balamt.broker.portal.ocr.exception;

public class FileStorageException extends Exception {
	
	public FileStorageException(String message, Exception ex) {
		super(message,ex);
	}

	public FileStorageException(String message) {
		super(message);
	}

}
