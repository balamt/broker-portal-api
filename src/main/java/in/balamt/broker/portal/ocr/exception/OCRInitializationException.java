package in.balamt.broker.portal.ocr.exception;

public class OCRInitializationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OCRInitializationException(String message) {
		super(message);
	}

	public OCRInitializationException(String message, Exception e) {
		super(message, e);
	}

}
