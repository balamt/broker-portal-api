package in.balamt.broker.portal.ocr.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import in.balamt.broker.portal.model.Deal;
import in.balamt.broker.portal.ocr.OcrProcessorService;
import in.balamt.broker.portal.ocr.exception.FileStorageException;
import in.balamt.broker.portal.ocr.exception.MyFileNotFoundException;
import in.balamt.broker.portal.ocr.exception.OCRInitializationException;

@RestController
@RequestMapping(value = "/deal")
public class DealController {

	public static final Logger LOGGER = Logger.getLogger(DealController.class.getName());

	@Autowired
	OcrProcessorService ocrProcessorService;

	@GetMapping("/test")
	public String testService() {
		return "Servie is up and running";
	}

	@PostMapping("/new")
	public ResponseEntity<Deal> uploadFile(@RequestParam("file") MultipartFile file)
			throws FileStorageException, OCRInitializationException {
		Deal deal = ocrProcessorService.getApplicationContent(file);
		return ResponseEntity.ok().body(deal);
	}

	@GetMapping("/{dealNumber}")
	public ResponseEntity<Deal> getDeal(@PathVariable("dealNumber") String dealNumber) throws IOException {
		return ResponseEntity.ok().body(ocrProcessorService.getDeal(dealNumber));
	}

	@GetMapping("/all")
	public ResponseEntity<List<String>> getAllFiles() throws IOException, MyFileNotFoundException {
		List<String> resource = ocrProcessorService.listAllFilesFromUploads();
		return ResponseEntity.ok().body(resource);
	}

	@GetMapping("/downloadFile/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request)
			throws MyFileNotFoundException {
		// Load file as Resource
		Resource resource = ocrProcessorService.loadFileAsResource(fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			LOGGER.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

}
