package in.balamt.broker.portal.ocr.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import in.balamt.broker.portal.model.Customer;
import in.balamt.broker.portal.model.Employment;
import in.balamt.broker.portal.model.LoanDetails;
import in.balamt.broker.portal.ocr.OcrProcessorServiceImpl;

public class OcrUtil {
	
	public static final Logger LOGGER = Logger.getLogger(OcrUtil.class.getName());

	private static final String NEW_LINE = "\n";
	private static final String EMPTY_STRING = "";
	private static final String $AUD = "$AUD";
	private static final String MM_DD_YYYY = "MM/dd/yyyy";
	private static final String SALARY_NET = "SALARY (NET) :";
	private static final String DESIGNATION = "DESIGNATION :";
	private static final String STARTED_WORKING_FROM = "STARTED WORKING FROM:";
	private static final String COMPANY_TYPE = "COMPANY TYPE :";
	private static final String COMPANY_NAME = "COMPANY NAME :";
	private static final String COMPANY_ADDRESS = "COMPANY ADDRESS :";

	private static final String ADDRESS = "ADDRESS :";
	private static final String MOBILE = "Mobile:";
	private static final String EMAIL = "EMAIL :";
	private static final String LAST_NAME = "LAST NAME :";
	private static final String FIRST_NAME = "FIRST NAME :";
	private static final String WORK = "WORK :";

	private static final String LOAN_TYPE = "LOAN TYPE :";
	private static final String LOAN_AMOUNT = "LOAN AMOUNT :";
	private static final String LOAN_TENURE = "LOAN TENURE :";
	private static final String INTREST = "INTREST :";

	private OcrUtil() {
	}

	public static Customer MapCustomerData(String rawData) {

		String[] stringArr = rawData.split(NEW_LINE);

		Customer customer = new Customer();

		for (String s : stringArr) {
			s = s.replace("_", EMPTY_STRING);
			if (s.contains(FIRST_NAME)) {
				customer.setFirstname(getData(s, FIRST_NAME));
			}
			if (s.contains(LAST_NAME)) {
				customer.setLastname(getData(s, LAST_NAME));
			}

			if (s.contains(EMAIL)) {
				customer.setEmail(getData(s, EMAIL));
			}

			if (s.contains(MOBILE)) {
				customer.setMobile(getData(s, MOBILE));
			}

			if (s.contains(WORK)) {
				customer.setWork(getData(s, WORK));
			}

			if (s.contains(ADDRESS)) {
				customer.setAddress(getData(s, ADDRESS));
			}
		}

		return customer;
	}

	public static Employment MapEmploymentData(String rawData) {

		String[] stringArr = rawData.split(NEW_LINE);

		Employment employment = new Employment();

		for (String s : stringArr) {
			s = s.replace("_", EMPTY_STRING);
			if (s.contains(COMPANY_NAME)) {
				employment.setCompanyName(getData(s, COMPANY_NAME));
			}
			if (s.contains(COMPANY_TYPE)) {
				employment.setCompanyType(getData(s, COMPANY_TYPE));
			}

			if (s.contains(STARTED_WORKING_FROM)) {
				String doj = getData(s, STARTED_WORKING_FROM);
				if (doj.trim().length() <= 0) {
					employment.setStartedWorkingFrom(null);
				} else {
					Date dateOfJoin;
					try {
						dateOfJoin = new SimpleDateFormat(MM_DD_YYYY).parse(doj);
						employment.setStartedWorkingFrom(dateOfJoin);
					} catch (ParseException e) {
					}
				}
			}

			if (s.contains(DESIGNATION)) {
				employment.setDesignation(getData(s, DESIGNATION).toUpperCase());
			}

			if (s.contains(SALARY_NET)) {

				String sal = getData(s, SALARY_NET).trim().replace($AUD, EMPTY_STRING);
				employment.setSalary(sal);
			}

			if (s.contains(COMPANY_ADDRESS)) {
				employment.setAddress(getData(s, COMPANY_ADDRESS));
			}
		}

		return employment;
	}

	public static LoanDetails MapLoanData(String rawData) {
		String[] stringArr = rawData.split(NEW_LINE);
		LoanDetails loanDetails = new LoanDetails();
		for (String s : stringArr) {
			s = s.replace("_", EMPTY_STRING);
			if (s.contains(LOAN_TYPE)) {
				loanDetails.setLoanType(getData(s, LOAN_TYPE).toUpperCase());
			}

			try {
				if (s.contains(LOAN_TENURE)) {
					loanDetails.setLoanTenure(Float.parseFloat(getData(s, LOAN_TENURE).trim()));
				}
				if (s.contains(LOAN_AMOUNT)) {
					loanDetails.setLoanAmount(Double.parseDouble(getData(s, LOAN_AMOUNT).trim()));
				}
				if (s.contains(INTREST)) {
					loanDetails.setIntrest(Float.parseFloat(getData(s, INTREST).trim()));
				}
			} catch (Exception e) {
				LOGGER.severe("Unable to Parse the Float or Double");
				LOGGER.severe(e.getMessage());
			}
		}
		return loanDetails;
	}

	private static String getData(String data, String type) {
		return data.replace(type, EMPTY_STRING).trim();
	}

}
